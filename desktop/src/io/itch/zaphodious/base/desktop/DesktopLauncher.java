package io.itch.zaphodious.base.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import io.itch.zaphodious.base.ZaphBase;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.height = 600;
        config.width = (int)(config.height * 0.5625f); // set the width of the screen to be the height times the desired aspect ratio

        buildMainAtlas();

		new LwjglApplication(new ZaphBase(), config);
	}

    /**
     * This function creates a TextureAtlas called "MasterAtlas.atlas" in the "atlas" folder, using the individual images in the "img" folder.
     */
    private static void buildMainAtlas() {
        TexturePacker.Settings settings = new TexturePacker.Settings();
        settings.maxWidth = 2048; // much larger and you will have a bad time on older hardware
        settings.maxHeight = 2048;
        settings.pot = true; // image dimensions should be powers of two? Yes. Makes it easier on the hardware.
        //settings.fast = true;
        settings.ignoreBlankImages = false; // I don't know why you'd even need a blank image in your atlas, but they'll be in there just in case.
        TexturePacker.process(settings, "img", "atlas", "MasterAtlas");


    }
}
