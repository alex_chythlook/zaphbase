# README #

Zaph's LibGDX base

### What is this repository for? ###

* Basically, this is an enhanced setup for LibGDX

### How do I get set up? ###

* Clone project via git
* Configure like any other LibGDX project
* Migrate it over to your own repo

Note- This is *not* a library. This is intended to be refactored and used as the start for a new project.

Short story time! I got tired of repeating the same boring-as-hell steps each time I wanted to begin a new project. I have wasted days of my life writing the same boilerplate over and over, and even while being fairly through I've ran into a number of problems because I'd forget a thing or two randomly between starts. I then decided to make a good starting point, where the basics are configured as I would normally like them to be. This is the result

I make this available to you without a warranty of any kind or a guarantee of fitness for a particular purpose, under the most recent Apache license. 