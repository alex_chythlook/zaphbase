package io.itch.zaphodious.base.screens

import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import io.itch.zaphodious.base.ServiceLocator
import io.itch.zaphodious.base.ashley.DrawnEntityBuilder
import io.itch.zaphodious.base.ui.UIManager

/**
 * Created by Alex on 1/14/2016.
 */
class TestingScreen(mainCamera: Camera, mainBatch: Batch, engine: PooledEngine) : UIEngineScreen(mainCamera,mainBatch,engine){


    override fun show() {
        super.show()

        uiManager.makeUI { stage, table ->
            table.add(TextButton("Testing!",ServiceLocator.skinGetter.mainUISkin)).center()
        }

        Gdx.input.inputProcessor = this.inputMultiplexer

        var entityBuilder = DrawnEntityBuilder(textureName = "badlogicAlt")
        entityBuilder injectEntityInto engine
    }

    override fun render(delta: Float) {
        super.render(delta)
    }

    override fun hide() {
        super.hide()
    }
}