package io.itch.zaphodious.base.screens

import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.viewport.FitViewport
import io.itch.zaphodious.base.ServiceLocator
import io.itch.zaphodious.base.ui.BasicUIManager
import io.itch.zaphodious.base.ui.UIManager

/**
 * Created by Alex on 1/14/2016.
 */
abstract class UIEngineScreen(mainCamera: Camera, mainBatch: Batch, engine: PooledEngine) : EngineScreen(mainCamera,mainBatch,engine) {

    val inputMultiplexer = InputMultiplexer()
    val uiManager: UIManager = BasicUIManager(mainBatch, mainCamera, inputMultiplexer)

    override fun show() {
        super.show()
        ServiceLocator.disposableCollector.addDisposable(uiManager)
    }

    override fun render(delta: Float) {
        super.render(delta)
        uiManager.update(delta)
        uiManager.draw()
    }

    override fun hide() {
        super.hide()
    }
}