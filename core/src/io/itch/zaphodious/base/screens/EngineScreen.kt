package io.itch.zaphodious.base.screens

import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteCache
import io.itch.zaphodious.base.ServiceLocator
import io.itch.zaphodious.base.ashley.DrawnEntityBuilder
import io.itch.zaphodious.base.ashley.addTo
import io.itch.zaphodious.base.ashley.components.SpriteComponent
import io.itch.zaphodious.base.ashley.systems.DrawingSystem
import io.itch.zaphodious.base.ashley.atPriority
import io.itch.zaphodious.base.ashley.components.AreaComponent
import io.itch.zaphodious.base.ashley.components.PositionComponent
import io.itch.zaphodious.base.ashley.systems.CullSystem
import io.itch.zaphodious.base.ashley.systems.MoveSystem
import io.itch.zaphodious.base.ashley.systems.SpriteAdjusterSystem

/**
 * Created by Alex on 1/13/2016.
 */
abstract class EngineScreen(var mainCamera: Camera, var mainBatch: Batch, var engine: PooledEngine) : ScreenAdapter() {

    val testingTexture = ServiceLocator.textureGetter.getRegion("badlogicAlt")



    override fun show() {
        super.show()
        println("building!")
        val testingSprite = Sprite(testingTexture)

        // Add the systems, using the lovely new syntax that is made available via extension functions
        // and infix notation


        SpriteAdjusterSystem()                                  addTo engine atPriority 0
        DrawingSystem(batch = mainBatch, camera = mainCamera)   addTo engine atPriority 1

        MoveSystem(mainCamera)                                  addTo engine atPriority 4

        CullSystem()                                            addTo engine atPriority 999

        /*var spriteComponent = engine.createComponent(SpriteComponent::class.java)
            spriteComponent.sprite = testingSprite
        var posComponent = engine.createComponent(PositionComponent::class.java)
            posComponent.position.set(100f,100f)
        var areaComponent = engine.createComponent(AreaComponent::class.java)
            areaComponent.height = 100f
            areaComponent.width = 100f

        engine.addEntity(engine.createEntity().add(spriteComponent).add(posComponent).add(areaComponent))*/



    }

    override fun hide() {
        super.hide()
    }

    override fun render(delta: Float) {
        super.render(delta)
        /*mainBatch.begin()
        mainBatch.draw(testingTexture,0f,0f)
        mainBatch.end()*/
        engine.update(delta)
    }

    override fun dispose() {
        super.dispose()
    }
}