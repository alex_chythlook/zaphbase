package io.itch.zaphodious.base

import io.itch.zaphodious.base.servicehelpers.*

/**
 * A collection of useful services, available anywhere. Variables here are set to dummy objects by default, and should be
 * enabled via assignment to actual implementations (such as UnifiedAssetManager) if the described functionality is desired.
 */
object ServiceLocator {
    var disposableCollector: DisposableCollector = DummyDisposableCollector()
    var textureGetter: TextureGetter = DummyTextureGetter()
    var soundPlayer: SoundPlayer = DummySoundPlayer()
    var skinGetter: SkinGetter = DummySkinGetter()
}