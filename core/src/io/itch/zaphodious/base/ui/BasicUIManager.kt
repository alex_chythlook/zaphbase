package io.itch.zaphodious.base.ui

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.viewport.FitViewport
import io.itch.zaphodious.base.ServiceLocator

/**
 * Created by Alex on 1/14/2016.
 */
class BasicUIManager (private val uiBatch: Batch = SpriteBatch(), private val uiCamera: Camera, private val multiplexer: InputMultiplexer) : UIManager{


    private val uiStage: Stage = Stage(FitViewport(uiCamera.viewportWidth, uiCamera.viewportHeight, uiCamera), uiBatch)
    private val mainTable: Table = Table(ServiceLocator.skinGetter.mainUISkin)

    init {
        mainTable.setPosition(0f,0f)
        mainTable.height = uiCamera.viewportHeight
        mainTable.width = uiCamera.viewportWidth
        uiStage.addActor(mainTable)
        multiplexer.addProcessor(0,this.uiStage)
    }

    override fun makeUI(uiMakeFun: (Stage, Table) -> Unit): UIManager {
        uiMakeFun(this.uiStage,this.mainTable)
        return this
    }

    override fun draw() = uiStage.draw()
    override fun update(deltaTime: Float) = uiStage.act(deltaTime)

    override fun dispose() {
        uiStage.dispose()
    }
}