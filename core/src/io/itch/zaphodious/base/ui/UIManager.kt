package io.itch.zaphodious.base.ui

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Disposable

/**
 * Designed to organize and encapsulate the process of creating UIs.
 */
interface UIManager : Disposable {

    /**
     * Designed to make setting up the UI self-documenting and painless, without having to have direct
     * access to the implementing class's members
     */
    fun makeUI(uiMakeFun: (stage:Stage, table:Table) -> Unit) : UIManager

    /**
     * Draws whatever needs to be drawn, independently of the update.
     */
    fun draw()

    /**
     * Updates the world/system/etc
     */
    fun update(deltaTime: Float)
}