package io.itch.zaphodious.base.ashley

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.PooledEngine
import io.itch.zaphodious.base.ashley.components.CullMarkerComponent

/**
 * Extensions intended to make working with Ashely a bit smoother
 */
infix fun EntitySystem.atPriority(newPriority: Int): EntitySystem {
    this.priority = newPriority
    return this
}

infix fun EntitySystem.addTo(engine: Engine): EntitySystem {
    engine.addSystem(this)
    return this
}

infix fun Entity.cullFrom(engine:Engine) {
    if (engine is PooledEngine) {
        this.add(engine.createComponent(CullMarkerComponent::class.java))
    } else {
        this.add(CullMarkerComponent())
    }
}