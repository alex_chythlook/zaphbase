package io.itch.zaphodious.base.ashley.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IntervalIteratingSystem
import io.itch.zaphodious.base.ashley.components.CullMarkerComponent

/**
 * Created by Alex on 1/19/2016.
 */
class CullSystem : IntervalIteratingSystem(Family.all(CullMarkerComponent::class.java).get(),1f/30f){
    override fun processEntity(entity: Entity?) {
        if (entity != null) {
            this.engine.removeEntity(entity)
        }
    }
}