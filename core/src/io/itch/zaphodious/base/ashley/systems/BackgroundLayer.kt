package io.itch.zaphodious.base.ashley.systems

/**
 * Created by Alex on 1/17/2016.
 */
enum class BackgroundLayer(var speed:Float) {
    MOST_BACK(1f),
    FAR_MID_BACK(10f),
    NEAR_MID_BACK(30f),
    MOST_NEAR(60f)
}