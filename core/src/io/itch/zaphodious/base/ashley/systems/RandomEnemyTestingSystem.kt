package io.itch.zaphodious.base.ashley.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.systems.IntervalSystem
import com.badlogic.gdx.graphics.Camera
import io.itch.zaphodious.base.ashley.DrawnEntityBuilder
import io.itch.zaphodious.base.ashley.components.EnemyComponent
import io.itch.zaphodious.base.ashley.components.VelocityComponent
import java.util.*

/**
 * Makes random enemies. Only usable with PooledEngine objects
 */
class RandomEnemyTestingSystem(val camera: Camera) : IntervalSystem(1f/3f){

    var enemyBuilders: MutableList<DrawnEntityBuilder> = ArrayList(10)
    val rand = Random()

    init {
        enemyBuilders.add(DrawnEntityBuilder(width = 124f,height = 122f,textureName = "shipBeige_manned"))
        enemyBuilders.add(DrawnEntityBuilder(width = 124f,height = 145f,textureName = "shipBlue_manned"))
        enemyBuilders.add(DrawnEntityBuilder(width = 124f,height = 123f,textureName = "shipGreen_manned"))
        enemyBuilders.add(DrawnEntityBuilder(width = 124f,height = 127f,textureName = "shipPink_manned"))
        enemyBuilders.add(DrawnEntityBuilder(width = 124f,height = 108f,textureName = "shipYellow_manned"))
    }

    override fun updateInterval() {
        val spawnDivisor = 6
        val spawnPointMultiplier = camera.viewportHeight / spawnDivisor
        fun makeEnemy(posY: Float) {
            var enemyBuilder = enemyBuilders.get(rand.nextInt(enemyBuilders.size-1))
            enemyBuilder.posX = camera.viewportWidth + enemyBuilder.width/2
            enemyBuilder.posY = posY//spawnPointMultiplier * rand.nextInt(spawnDivisor) + 1

            val velComp = (engine as PooledEngine).createComponent(VelocityComponent::class.java)
            velComp.angle = 90f
            velComp.speed = 40f + (rand.nextFloat()-rand.nextFloat()) + (rand.nextInt(10) - rand.nextInt(10))

            val enemyComp = (engine as PooledEngine).createComponent(EnemyComponent::class.java)

            (enemyBuilder injectEntityInto engine as PooledEngine).add(velComp).add(enemyComp)
        }

        for (i in 1..(spawnDivisor-1)) {
            if (rand.nextBoolean() && rand.nextBoolean()) {
                makeEnemy(i * spawnPointMultiplier)
            }
        }


    }

    override fun addedToEngine(engine: Engine?) {
        if (engine !is PooledEngine) {
            throw TypeCastException("{$javaClass} is only compatible with PooledEngine objects")
        }
        super.addedToEngine(engine)
    }
}