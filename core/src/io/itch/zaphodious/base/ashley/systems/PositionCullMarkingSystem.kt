package io.itch.zaphodious.base.ashley.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.systems.IntervalIteratingSystem
import com.badlogic.gdx.graphics.Camera
import io.itch.zaphodious.base.ashley.AshMappers
import io.itch.zaphodious.base.ashley.components.CullMarkerComponent
import io.itch.zaphodious.base.ashley.components.PositionComponent
import io.itch.zaphodious.base.ashley.cullFrom

/**
 * Created by Alex on 1/18/2016.
 */
class PositionCullMarkingSystem(val camera: Camera) : IntervalIteratingSystem(Family.all(PositionComponent::class.java).get(),1f/10f){
    override fun processEntity(entity: Entity?) {
        if (entity != null) {
            val posComp = AshMappers.positionMapper.get(entity)

            if (posComp.position.x < -(camera.viewportWidth) *0.5) entity cullFrom this.engine
            if (posComp.position.x >  (camera.viewportWidth) *1.5) entity cullFrom this.engine
            if (posComp.position.y < -(camera.viewportHeight)*0.5) entity cullFrom this.engine
            if (posComp.position.y >  (camera.viewportHeight)*1.5) entity cullFrom this.engine
        }
    }
}