package io.itch.zaphodious.base.ashley.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import io.itch.zaphodious.base.ashley.AshMappers
import io.itch.zaphodious.base.ashley.components.AreaComponent
import io.itch.zaphodious.base.ashley.components.PositionComponent
import io.itch.zaphodious.base.ashley.components.SpriteComponent

/**
 * Created by Alex on 1/13/2016.
 */
class SpriteAdjusterSystem :
        IteratingSystem(Family.all(SpriteComponent::class.java,
                PositionComponent::class.java,
                AreaComponent::class.java).get()) {

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        if (entity != null) {
            val positionComp = AshMappers.positionMapper.get(entity)
            val areaComp = AshMappers.areaMapper.get(entity)
            val spriteComp = AshMappers.spriteMapper.get(entity)

            val spritePosX = positionComp.position.x - (areaComp.width / 2f)
            val spritePosY = positionComp.position.y - (areaComp.height / 2f)



            //println("setting sprite's bounds to $spritePosX, $spritePosY with area ${areaComp.width}x${areaComp.height}")

            //spriteComp.sprite.setBounds(spritePosX,spritePosY,areaComp.width,areaComp.height)
            spriteComp.sprite.setPosition(spritePosX, spritePosY)
            spriteComp.sprite.setSize(areaComp.width, areaComp.height)
            spriteComp.sprite.setOriginCenter()

            if (spriteComp.rotateWithAngle && AshMappers.velocityMapper.has(entity)) {
                val velComp = AshMappers.velocityMapper.get(entity)
                spriteComp.sprite.rotation = velComp.angle
            }
        }
    }
}