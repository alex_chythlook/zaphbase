package io.itch.zaphodious.base.ashley.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.brashmonkey.spriter.Spriter
import io.itch.zaphodious.base.ServiceLocator
import io.itch.zaphodious.base.ashley.AshMappers
import io.itch.zaphodious.base.ashley.components.SpriteComponent
import java.util.*
import kotlin.collections.forEach

/**
 * Created by Alex on 1/13/2016.
 */
class DrawingSystem(val batch: Batch, val camera: Camera) : EntitySystem() {

    val backgroundSprite = Sprite(ServiceLocator.textureGetter.getRegion("background"))
    val backgroundAR = backgroundSprite.width / backgroundSprite.height
    val drawingFamily = Family.all(SpriteComponent::class.java).get()
    val foregroundDrawingList:MutableList<Entity> = ArrayList(1000000)



    override fun update(deltaTime: Float) {

        // First, we sort out all of the background elements. If it's not a background element, it gets thrown into the main rendering list
        engine.getEntitiesFor(drawingFamily).forEach {
                foregroundDrawingList.add(it)
            }


        batch.begin()
        backgroundSprite.setBounds(0f,0f,(camera.viewportHeight*1.2f) * backgroundAR, (camera.viewportHeight*1.2f)) // The main background is just a static image
        backgroundSprite.draw(batch)

        foregroundDrawingList.forEach { processEntity(it,deltaTime) } // draw everything else, as they were thrown into the list

        batch.end()

        foregroundDrawingList.clear() // clear the foreground list
    }

    private fun processEntity(entity: Entity?, deltaTime: Float) {
        if (entity != null) {
            if (AshMappers.cullMarker.has(entity)) { // We don't draw entities that are set to be culled.
                return
            }
            AshMappers.spriteMapper.get(entity)?.sprite?.draw(batch)
        }
    }
}