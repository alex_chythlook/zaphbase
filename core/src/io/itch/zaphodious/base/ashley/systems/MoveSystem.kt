package io.itch.zaphodious.base.ashley.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Camera
import io.itch.zaphodious.base.ashley.AshMappers
import io.itch.zaphodious.base.ashley.components.PositionComponent
import io.itch.zaphodious.base.ashley.components.VelocityComponent

/**
 * Created by Alex on 1/17/2016.
 */
class MoveSystem(val mainCamera: Camera) : IteratingSystem(Family.all(PositionComponent::class.java, VelocityComponent::class.java).get()) {
    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val posComp = AshMappers.positionMapper.get(entity)
        val velosComp = AshMappers.velocityMapper.get(entity)

        val totalToMove = (velosComp.speed * 10f) * deltaTime
        val rads = Math.toRadians(velosComp.angle.toDouble() + 180)

        val toMoveX = totalToMove * Math.sin(rads).toFloat()
        val toMoveY = totalToMove * Math.cos(rads).toFloat()

        if (!AshMappers.playerMapper.has(entity)) { posComp.position.x += toMoveX } // We don't like the player moving on the X
        posComp.position.y += toMoveY

        if (AshMappers.playerMapper.has(entity)) {
            // do some player-specific stuff. Why not make a new system? Because why bother, if we already have everything here?
            if (posComp.position.y > mainCamera.viewportHeight) {
                posComp.position.y = mainCamera.viewportHeight
            }
            if (posComp.position.y < 0) {
                posComp.position.y = 0f
            }
        }

    }
}