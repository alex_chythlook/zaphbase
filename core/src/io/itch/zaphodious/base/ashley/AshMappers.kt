package io.itch.zaphodious.base.ashley

import com.badlogic.ashley.core.ComponentMapper
import io.itch.zaphodious.base.ashley.components.*

/**
 * One-stop shop for pre-set ComponentMappers. New components should have a corresponding mapper added to this Object
 */
object AshMappers {
    var spriteMapper = ComponentMapper.getFor(SpriteComponent::class.java)
    var positionMapper = ComponentMapper.getFor(PositionComponent::class.java)
    var areaMapper = ComponentMapper.getFor(AreaComponent::class.java)
    var playerMapper = ComponentMapper.getFor(PlayerComponent::class.java)
    var velocityMapper = ComponentMapper.getFor(VelocityComponent::class.java)
    var cullMarker = ComponentMapper.getFor(CullMarkerComponent::class.java)
    var animationMapper = ComponentMapper.getFor(AnimationComponent::class.java)
}