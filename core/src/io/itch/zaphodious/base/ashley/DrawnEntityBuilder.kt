package io.itch.zaphodious.base.ashley

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.graphics.g2d.Sprite
import io.itch.zaphodious.base.ashley.components.AreaComponent
import io.itch.zaphodious.base.ashley.components.PositionComponent
import io.itch.zaphodious.base.ashley.components.SpriteComponent
import io.itch.zaphodious.base.ServiceLocator
import io.itch.zaphodious.base.ashley.components.VelocityComponent
import io.itch.zaphodious.base.ashley.systems.BackgroundLayer

/**
 * Creates drawable entities using default components.
 */
class DrawnEntityBuilder(var width: Float = 100f,
                         var height: Float = 100f,
                         var posX: Float = 100f,
                         var posY: Float = 100f,
                         var speed: Float = 0f,
                         var angle: Float = 0f,
                         var textureName: String = "badlogic") {

    var rotateWithAngle = false

    fun makeEntity(engine: PooledEngine): Entity {
        val entity = engine.createEntity()

        var spriteComponent = engine.createComponent(SpriteComponent::class.java)
        spriteComponent.sprite = Sprite(ServiceLocator.textureGetter.getRegion(textureName))
        spriteComponent.rotateWithAngle = rotateWithAngle
        var posComponent = engine.createComponent(PositionComponent::class.java)
        posComponent.position.set(posX, posY)
        var areaComponent = engine.createComponent(AreaComponent::class.java)
        areaComponent.height = height
        areaComponent.width = width
        var velocityComponent = engine.createComponent(VelocityComponent::class.java)
        velocityComponent.angle = this.angle
        velocityComponent.speed = this.speed

        entity.add(spriteComponent).add(posComponent).add(areaComponent).add(velocityComponent)

        return entity
    }

    infix fun injectEntityInto(engine: PooledEngine): Entity {
        val entity = makeEntity(engine)
        engine.addEntity(entity)
        return entity
    }

}