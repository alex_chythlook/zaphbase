package io.itch.zaphodious.base.ashley.components

import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.math.Vector2

/**
 * Created by Alex on 1/13/2016.
 */
class PositionComponent : PooledComponent, CloneableComponent {

    val position: Vector2 = Vector2(0f, 0f)

    override fun duplicate(pooledEngine: PooledEngine): CloneableComponent {
        val toReturn = pooledEngine.createComponent(this.javaClass)

        toReturn.position.set(this.position)

        return toReturn
    }

    override fun reset() {
        position.set(0f, 0f)
    }
}