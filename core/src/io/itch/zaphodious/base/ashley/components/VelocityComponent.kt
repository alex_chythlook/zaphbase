package io.itch.zaphodious.base.ashley.components

import com.badlogic.ashley.core.PooledEngine

/**
 * Created by Alex on 1/17/2016.
 */
class VelocityComponent : PooledComponent, CloneableComponent {

    var speed = 100f
    var angle = 0f

    override fun duplicate(pooledEngine: PooledEngine): CloneableComponent {
        val toReturn = pooledEngine.createComponent(this.javaClass)

        toReturn.angle = this.angle
        toReturn.speed = this.speed

        return toReturn
    }

    override fun reset() {
        speed = 100f
        angle = 0f
    }
}