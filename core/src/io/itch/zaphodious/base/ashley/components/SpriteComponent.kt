package io.itch.zaphodious.base.ashley.components

import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.graphics.g2d.Sprite

/**
 * Created by Alex on 1/13/2016.
 */

class SpriteComponent : PooledComponent, CloneableComponent {


    private var _sprite: Sprite = Sprite()
    /**
     * We only keep one sprite object in memory, overriding the sprite field's set() method to delate to the sprite, letting
     * it set itself.
     */
    var sprite: Sprite
        get() = _sprite
        set(value) {
            _sprite.set(value)
            _sprite.setOriginCenter()
        }

    var rotateWithAngle = false

    override fun duplicate(pooledEngine: PooledEngine): CloneableComponent {
        val toReturn = pooledEngine.createComponent(this.javaClass)
        toReturn.sprite = this.sprite
        return toReturn
    }

    override fun reset() {
        rotateWithAngle = false
    }
}