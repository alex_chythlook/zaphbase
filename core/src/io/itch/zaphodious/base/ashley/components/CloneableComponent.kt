package io.itch.zaphodious.base.ashley.components

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.PooledEngine

/**
 * A "Component" class implementing this interface needs to be able to make a field-for-field
 * independent copy of itself.
 */
interface CloneableComponent : Component {
    /**
     * Creates a field-for-field copy of the implementing Component
     */
    fun duplicate(pooledEngine: PooledEngine) : CloneableComponent
}