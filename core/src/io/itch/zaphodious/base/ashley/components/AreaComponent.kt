package io.itch.zaphodious.base.ashley.components

import com.badlogic.ashley.core.PooledEngine

/**
 * Created by Alex on 1/13/2016.
 */
class AreaComponent : PooledComponent, CloneableComponent {

    var width = 20f
    var height = 20f

    override fun duplicate(pooledEngine: PooledEngine): CloneableComponent {
        val toReturn = pooledEngine.createComponent(this.javaClass)

        toReturn.height = this.height
        toReturn.width = this.width

        return toReturn
    }

    override fun reset() {
        width = 20f
        height = 20f
    }
}