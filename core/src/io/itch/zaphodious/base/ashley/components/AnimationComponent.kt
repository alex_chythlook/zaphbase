package io.itch.zaphodious.base.ashley.components

import com.badlogic.gdx.graphics.g2d.Animation

/**
 * Created by Alex on 1/23/2016.
 */
class AnimationComponent : PooledComponent {

    var animation: Animation? = null

    override fun reset() {
        this.animation = null
    }

}