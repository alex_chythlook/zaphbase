package io.itch.zaphodious.base.ashley.components

/**
 * Created by Alex on 1/17/2016.
 */
class PlayerComponent : PooledComponent {

    var maxPositiveVelocity = 5000f
    var minPositiveVelocity = 100f
    var currentPositiveVelocity = 100f

    override fun reset() {
        maxPositiveVelocity = 1000f
        minPositiveVelocity = 100f
        currentPositiveVelocity = 100f
    }
}