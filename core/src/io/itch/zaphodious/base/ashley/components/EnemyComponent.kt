package io.itch.zaphodious.base.ashley.components

/**
 * Created by Alex on 1/19/2016.
 */
class EnemyComponent : PooledComponent {

    var maxHealth = 100f
    var currentHealth = 100f

    override fun reset() {
        maxHealth = 100f
        currentHealth = 100f
    }
}