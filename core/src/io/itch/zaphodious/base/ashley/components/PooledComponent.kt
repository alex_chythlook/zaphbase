package io.itch.zaphodious.base.ashley.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool

/**
 * Created by Alex on 1/13/2016.
 */
interface PooledComponent : Pool.Poolable, Component {
}