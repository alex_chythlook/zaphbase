package io.itch.zaphodious.base.ashley.listeners

import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.Family

/**
 * Created by Alex on 1/19/2016.
 */
interface EasyEntityListener : EntityListener {
    val preferredFamily: Family
}