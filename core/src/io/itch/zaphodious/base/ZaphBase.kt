package io.itch.zaphodious.base

import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import io.itch.zaphodious.base.screens.EngineScreen
import io.itch.zaphodious.base.ServiceLocator
import io.itch.zaphodious.base.screens.TestingScreen
import io.itch.zaphodious.base.servicehelpers.UnifiedAssetHandler

/**
 * Created by Alex on 1/13/2016.
 */
class ZaphBase : Game() {
    lateinit var batch: SpriteBatch
    lateinit var camera: OrthographicCamera

    override fun create() {

        // Before doing anything else, we need to set our services to something that will actually work when called.
        val unifiedAssetHandler = UnifiedAssetHandler()

        ServiceLocator.disposableCollector = unifiedAssetHandler
        ServiceLocator.textureGetter = unifiedAssetHandler
        ServiceLocator.soundPlayer = unifiedAssetHandler
        ServiceLocator.skinGetter = unifiedAssetHandler

        // Now, lets set it up so that we can actually see everything correctly.
        // First, figure out AR
        val ar: Float = Gdx.graphics.width.toFloat() / Gdx.graphics.height.toFloat()
        // Now, we figure out what our viewport's height is
        val viewHeight = 1000f
        // Now, we use the two values to figure out our viewport's width
        val viewWidth = viewHeight * ar


        // Now, use the calculated info to configure the camera
        camera = OrthographicCamera()
        camera.setToOrtho(false, viewWidth,viewHeight)

        // The main spritebatch used by our project
        batch = SpriteBatch()

        // we set up the default screen, which is where all the other magic will happen
        this.setScreen(TestingScreen(mainCamera = camera, mainBatch = batch, engine = PooledEngine()))
    }

    override fun render() {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        // We adjust the batch so that other classes don't have to.
        camera.update()
        batch.setProjectionMatrix(camera.combined)
        super.render()

    }

    override fun dispose() {
        super.dispose()
        ServiceLocator.disposableCollector.dispose()
    }
}