package io.itch.zaphodious.base.servicehelpers

import com.badlogic.gdx.utils.Disposable

/**
 * Collects disposable elements, to make it easier to properly dispose() of them when the time comes
 */
interface DisposableCollector : Disposable {
    /**
     * Accepts any object, because not every object that is disposable will implement the interface. Clients should
     * implement appropriate logic to ensure that any applicable object can be tracked and disposed of.
     */
    infix fun addDisposable(value: Any)
}

