package io.itch.zaphodious.base.servicehelpers

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Skin

/**
 * Dummy classes, existing so that we have something inoffensive and light to give to ServiceLocator on
 * initialization. References to these objects should be replaced with other, more useful(!) objects in order
 * for any functionality to be achieved. If a certain service is to be suspended, however, leaving
 * the dummy implementations should effectively cut it off.
 */


/**
 * Dummy implementation of TextureGetter. Method returns the badlogic logo
 */
class DummyTextureGetter : TextureGetter {

    override fun getRegion(regionName: String): TextureRegion {
        return badlogicLogo
    }

}

/**
 * Exists to give DummyTextureGetters something to return, while also allowing for its disposable without having to
 * worry about the dummy object's own disposal. UnifiedAssetHandler objects will automatically handle said disposal,
 * but other implementations of DisposableCollector might not.
 */
val badlogicLogo = TextureRegion(Texture("badlogic.jpg"))

/**
 * Dummy implementation of DisposableCollector. Methods do nothing.
 */
class DummyDisposableCollector : DisposableCollector {

    override fun dispose() {
    }

    override fun addDisposable(value: Any) {

    }

}

/**
 * Dummy implementation of SoundPlayer. Play methods return 0, otherwise the methods do nothing.
 */
class DummySoundPlayer : SoundPlayer {
    override fun playSound(soundName: String): Long {
        return 0
    }

    override fun stopSound(soundID: Long) {
    }

    override fun playMusic(musicName: String): Long {
        return 0
    }

    override fun stopMusic() {
    }
}


class DummySkinGetter : SkinGetter {
    override val mainUISkin: Skin = Skin()

}