package io.itch.zaphodious.base.servicehelpers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Disposable
import java.util.*

/**
 * Standard implementation of asset-concerned service interfaces.
 */
class UnifiedAssetHandler : AssetManager(), DisposableCollector, TextureGetter, SoundPlayer, SkinGetter {
    override val mainUISkin: Skin = Skin(Gdx.files.internal("skin/uiskin.json"))

    val mainAtlas = TextureAtlas("atlas/MasterAtlas.atlas")

    val listOfDisposableThings = ArrayList<Disposable>(100000)

    init {
        this addDisposable mainAtlas
        this addDisposable mainUISkin
        this addDisposable badlogicLogo // We make sure that the badlogic logo is disposed of properly
    }


    override fun addDisposable(value: Any) {
        if (value is Disposable) {
            this.listOfDisposableThings.add(value)
        }
    }

    override fun getRegion(regionName: String): TextureRegion {
        return mainAtlas.findRegion(regionName)
    }

    override fun playSound(soundName: String): Long {
        return 0
    }

    override fun stopSound(soundID: Long) {
    }

    override fun playMusic(musicName: String): Long {
        return 0
    }

    override fun stopMusic() {
    }

    override fun dispose() {
        super.dispose()
        listOfDisposableThings.forEach { it.dispose() }
    }

}