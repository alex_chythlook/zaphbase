package io.itch.zaphodious.base.servicehelpers

import com.badlogic.gdx.scenes.scene2d.ui.Skin

/**
 * Created by Alex on 1/13/2016.
 */
interface SkinGetter {
    val mainUISkin: Skin
}