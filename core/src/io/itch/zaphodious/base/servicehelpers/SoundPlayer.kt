package io.itch.zaphodious.base.servicehelpers

/**
 * Instances of this class will play and stop sounds and music
 */
interface SoundPlayer {
    fun playSound(soundName: String): Long
    fun stopSound(soundID: Long)
    fun playMusic(musicName: String): Long
    fun stopMusic()
}