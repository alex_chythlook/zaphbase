package io.itch.zaphodious.base.servicehelpers

import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
 * Instances of this class will provide a texture region for a given String
 */
interface TextureGetter {
    fun getRegion(regionName: String): TextureRegion
}

